<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Cart
{
		public function __construct()
		{
			$this->hide_cart_postcode = get_option('woohacked_hide_cart_postcode');
			$this->hide_cart_city = get_option('woohacked_hide_cart_city');
			$this->hide_cart_state = get_option('woohacked_hide_cart_state');
			$this->hide_cart_country = get_option('woohacked_hide_cart_country');
			$this->hide_cart_options();
			$this->init();	
		}
		public function init()
		{
			add_filter( 'woocommerce_locate_template', array($this, 'template'), 10, 3);
			add_action('woohacked_coordinates_field', array($this, 'field'));
			add_filter( 'woocommerce_shipping_settings', array(&$this, 'hide_cart_fields'));
		}
		public function hide_cart_options()
		{
			if($this->hide_cart_postcode == 'yes')
			{
				add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );
			}
			if($this->hide_cart_city == 'yes')
			{
				add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
			}
			if($this->hide_cart_state == 'yes')
			{
				add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false' );
			}
			if($this->hide_cart_country == 'yes')
			{
				add_filter( 'woocommerce_shipping_calculator_enable_country', '__return_false' );
			}			
		}
		
		public function field()
		{
			?>
			<p class="woohacked-coordinates-field hidden">
				<label><?php esc_html_e('Find the exact address on the map', 'woohacked'); ?></label>
				<span>
					<input onclick="woohacked_modal()" type="text" class="input-text" placeholder="<?php esc_html_e('Click here to open map...', 'woohacked'); ?>" name="woohacked_client_coordinates" id="woohacked_client_coordinates" />
				</span>
			</p>
			<input type="hidden" name="has_no_coordinates" id="has_no_coordinates" value="" / >
			<?php
		}

		function template( $template, $template_name, $template_path )
		{
			if($template_name == 'cart/shipping-calculator.php')
			{
				$plugin_path  = untrailingslashit( plugin_dir_path( __FILE__ ) )  . '/templates/';
				$template = $plugin_path . $template_name;				
			}
			
			return $template;
		}
		
	public function hide_cart_fields($settings)
	{
		$settings[] =  array(
          'title' => __( 'Cart Settings', 'woohacked' ),
          'type'  => 'title',
          'id'    => 'woohacked',
        );			
		$settings[] = array(
			'title' => __('Hide postcode from the cart', 'woohacked'),
			'type' => 'checkbox',
			'id' => 'woohacked_hide_cart_postcode',
			'default' => 'no'
		);
		$settings[] = array(
			'title' => __('Hide city from the cart', 'woohacked'),
			'type' => 'checkbox',
			'id' => 'woohacked_hide_cart_city',
			'default' => 'no'
		);
		$settings[] = array(
			'title' => __('Hide state from the cart', 'woohacked'),
			'type' => 'checkbox',
			'id' => 'woohacked_hide_cart_state',
			'default' => 'no'
		);	
		$settings[] = array(
			'title' => __('Hide country from the cart', 'woohacked'),
			'type' => 'checkbox',
			'id' => 'woohacked_hide_cart_country',
			'default' => 'no'
		);	
		$settings[] =  array(
          'type'  => 'sectionend',
          'id'    => 'woohacked',
        );			
		
		return $settings;
	}		
}

