<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Google_Map
{
	public function __construct($settings, $api_endpoint)
	{
		$this->api_endpoint = $api_endpoint;
		$this->init();
	}
	public function init()
	{
		add_action('wp_footer', array(&$this, 'map_modal'));
		add_action('wp_enqueue_scripts', array(&$this, 'enqueue_scripts'), 10);
		add_filter('woohacked_args', array(&$this, 'args'));
	}
	public function enqueue_scripts()
	{
		global $WOOHACKED_STORE_CONFIG;
		
		if(isset($WOOHACKED_STORE_CONFIG))
		{
			if(is_checkout() || is_cart())
			{
				$url = plugin_dir_url( __FILE__ );
				
				wp_enqueue_script('woohacked-coordinates', $url . 'coordinates.js', array('jquery'), time(), true);
				wp_enqueue_script('woohacked-map', 'https://maps.googleapis.com/maps/api/js?key='.$WOOHACKED_STORE_CONFIG['google_maps_key'].'&callback=initMap&libraries=places,geometry,drawing', array('jquery', 'woohacked-coordinates'), '', true);
			}			
		}
	}
	public function args($args)
	{
		global $WOOHACKED_STORE_CONFIG;
		
		if(isset($WOOHACKED_STORE_CONFIG))
		{
			if(is_checkout() || is_cart())
			{	
				if(array_key_exists('glovo_city_code', $WOOHACKED_STORE_CONFIG))
				{
					$args['polygons_endpoint'] = esc_url($this->api_endpoint.'public/glovo_polygons/'.$WOOHACKED_STORE_CONFIG['glovo_city_code']);
				}
				
				$args['gmk'] = $WOOHACKED_STORE_CONFIG['google_maps_key'];
				
				return $args;			
			}			
		}
	}
	public function map_modal()
	{	
		if(is_checkout() || is_cart())
		{
			?>
				<div id="woohacked-modal" class="hidden">
					<div class="modal-container">
					
						<div class="header">
							<div class="left">
								<span class="title"><?php esc_html_e('Find the exact address on the map', 'woohacked'); ?></span>
							</div>
							<div class="right">
								<span class="close">X</span>
							</div>
						</div>
						<p class="woohacked-coordinates-field hidden">
							<span>
							<input class="input-text" type="text" id="woohacked-search" />
							</span>
						</p>
						<div id="woohacked-map">
							<p><button id="woohacked-share-location" type="button"><?php esc_html_e('Share your location', 'woohacked'); ?></button></p>
						</div>
						<div class="footer">
							<button class="modal-btn" type="button" disabled="disabled"><?php esc_html_e('Confirm address', 'woohacked'); ?></button>
						</div>
					</div>
				</div>
				<div id="woohacked-overlay" class="hidden">
			<?php			
		}
	}
}

