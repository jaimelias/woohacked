<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Speed
{
	public function __construct()
	{
		$this->init();
	}
	
	public function init()
	{
		add_filter( 'script_loader_tag', array($this, 'async_defer'), 999, 3 );
		add_action('wp_enqueue_scripts', array($this, 'sentry'), 10);
		add_action('wp_enqueue_scripts', array($this, 'mailchimp'), 10);
		add_action('wp_enqueue_scripts', array($this, 'dequeue'), 999);
		$this->performance();
		$this->disable_embeds_code_init();
	}
	public function async_defer($tag, $handle, $src)
	{
		if(preg_match("/async/i", $src) || preg_match("/defer/i", $src))
		{
			$method = '';
			
			if(preg_match("/async/i", $src))
			{
				$method .= ' async ';
			}
			if(preg_match("/defer/i", $src))
			{
				$method .= ' defer ';
			}
			
			$tag = '<script id="'.esc_html($handle).'" type="text/javascript" '.esc_html($method).' src="'.esc_url($src).'"></script>';
		}
		return $tag;
	}	
	public function performance()
	{
		add_filter( 'jetpack_lazy_images_blocked_classes', array(&$this, 'not_lazy'), 999, 1 );
		add_filter( 'jetpack_implode_frontend_css', '__return_false' );
		add_action('wp_print_styles', array(&$this, 'remove_jetpack_styles'));
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'feed_links_extra', 3 ); 
		remove_action( 'wp_head', 'rest_output_link_wp_head');
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links');
		remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
		remove_action ('wp_head', 'rsd_link');
		remove_action( 'wp_head', 'wlwmanifest_link');
		remove_action( 'wp_head', 'wp_shortlink_wp_head');
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );		
	}
	
	public function not_lazy($classes)
	{
	   $classes[] = 'custom-logo';
	   return $classes;		
	}
	
	public function sentry()
	{
		if(is_cart() || is_checkout())
		{
			$sentry_url = 'https://browser.sentry-cdn.com/5.15.5/bundle.min.js';
			$sentry_init = 'Sentry.init({ dsn: "https://d45e02e27e7143cd97143e0d80b000d5@o387185.ingest.sentry.io/5222125" });';
			
			wp_enqueue_script('sentry_io', $sentry_url, array(), '');
			wp_add_inline_script('sentry_io', $sentry_init);			
		}		
	}
	
	public function mailchimp()
	{
		global $wp_scripts;
		
		if(isset($wp_scripts))
		{
			if(is_object($wp_scripts))
			{
				$mailchimp;
				$mailchimp_script = 'mailchimp-woocommerce';
				
				foreach($wp_scripts->queue as $handle )
				{
					if($handle == $mailchimp_script)
					{
						$wp_scripts->registered[$handle]->ver = 'async_defer';
					}
				}				
			}
		}
	}

	public function remove_jetpack_styles()
	{
		wp_deregister_style( 'AtD_style' ); // After the Deadline
		wp_deregister_style( 'jetpack_likes' ); // Likes
		wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
		wp_deregister_style( 'jetpack-carousel' ); // Carousel
		wp_deregister_style( 'grunion.css' ); // Grunion contact form
		wp_deregister_style( 'the-neverending-homepage' ); // Infinite Scroll
		wp_deregister_style( 'infinity-twentyten' ); // Infinite Scroll - Twentyten Theme
		wp_deregister_style( 'infinity-twentyeleven' ); // Infinite Scroll - Twentyeleven Theme
		wp_deregister_style( 'infinity-twentytwelve' ); // Infinite Scroll - Twentytwelve Theme
		wp_deregister_style( 'noticons' ); // Notes
		wp_deregister_style( 'post-by-email' ); // Post by Email
		wp_deregister_style( 'publicize' ); // Publicize
		wp_deregister_style( 'sharedaddy' ); // Sharedaddy
		wp_deregister_style( 'sharing' ); // Sharedaddy Sharing
		wp_deregister_style( 'stats_reports_css' ); // Stats
		wp_deregister_style( 'jetpack-widgets' ); // Widgets
		wp_deregister_style( 'jetpack-slideshow' ); // Slideshows
		wp_deregister_style( 'presentations' ); // Presentation shortcode
		wp_deregister_style( 'jetpack-subscriptions' ); // Subscriptions
		wp_deregister_style( 'tiled-gallery' ); // Tiled Galleries
		wp_deregister_style( 'widget-conditions' ); // Widget Visibility
		wp_deregister_style( 'jetpack_display_posts_widget' ); // Display Posts Widget
		wp_deregister_style( 'gravatar-profile-widget' ); // Gravatar Widget
		wp_deregister_style( 'widget-grid-and-list' ); // Top Posts widget
		wp_deregister_style( 'jetpack-widgets' ); // Widgets		
	}
	public function dequeue()
	{
		wp_enqueue_style('woohacked-google-map', plugin_dir_url( __FILE__ ) . 'style.css', array(), '', 'all');
		wp_dequeue_style('wp-block-library');
		wp_dequeue_style('wp-block-library-theme');
		wp_dequeue_style('wc-block-style');
		wp_dequeue_style('storefront-jetpack-widgets');
		wp_dequeue_style('storefront-gutenberg-blocks');
		wp_dequeue_style( 'storefront-fonts' );
	}
	function disable_embeds_code_init() {

	 remove_action( 'rest_api_init', 'wp_oembed_register_route' );
	 add_filter( 'embed_oembed_discover', '__return_false' );
	 remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
	 remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	 remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	 add_filter( 'tiny_mce_plugins', array(&$this, 'disable_embeds_tiny_mce_plugin') );
	 add_filter( 'rewrite_rules_array', array(&$this, 'disable_embeds_rewrites') );
	 remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
	}

	public function disable_embeds_tiny_mce_plugin($plugins) {
		return array_diff($plugins, array('wpembed'));
	}

	public function disable_embeds_rewrites($rules) {
		foreach($rules as $rule => $rewrite) {
			if(false !== strpos($rewrite, 'embed=true')) {
				unset($rules[$rule]);
			}
		}
		return $rules;
	}
		
}