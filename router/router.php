<?php

if (!defined('ABSPATH'))
{
    return;
}

class WP_Dynamic_Router 
{	

	public $routes;
	
	public function __construct($routes)
	{
		if(is_array($routes))
		{
			if(count($routes) > 0)
			{
				$this->routes = $routes;
				$this->init();				
			}
		}
	}
	
	public function init()
	{
		if($this->routes)
		{
			add_action( 'init', array( &$this, 'rewrite' ));
			add_action( 'init', array( &$this, 'tags' ));
			add_filter( 'template_include', array( &$this, 'template' ), 99);
			add_filter( 'query_vars', array( &$this, 'query_vars' ), 10, 1);
			add_filter( 'post_link', array( &$this, 'post_link' ), 11, 3);			
		}
	}	

	public function get_query_obj($query_var)
	{
		$routes = $this->routes;
		$output = false;

		
		
		if(get_query_var($query_var))
		{			
			$output = get_query_var($query_var);
		}
		else
		{
			if(array_key_exists($query_var, $routes))
			{
				if(array_key_exists( 'add_lang', $routes[$query_var]))
				{
					foreach($routes[$query_var]['add_lang'] as $k2 => $v2)
					{				
						if(get_query_var($v2))
						{
							$output = get_query_var($v2);
						}
					}				
				}				
			}
		}

		return $output;
	}
	
	public function has_request()
	{
		$routes = $this->routes;
		$valid = false;
		$obj = (object) array();
		$output = false;
		global $wp;
		
		
		if(isset($wp))
		{
			if(property_exists($wp, 'request') && property_exists($wp, 'query_vars'))
			{
				
				foreach( $routes as $k => $v )
				{
					if($wp->query_vars)
					{
						if( array_key_exists($k, $wp->query_vars) )
						{
							$valid = true;
						}
						else
						{
							if(array_key_exists('add_lang', $routes[$k]))
							{
								foreach($routes[$k]['add_lang'] as $k2 => $v2)
								{	
									if(array_key_exists($v2, $wp->query_vars))
									{
										$valid = true;
									}
								}
								
							}
						}						
					}
				}
				
				if($valid === true)
				{
					$obj->query_vars = $wp->query_vars;
					$obj->request = $wp->request;
					$output = $obj;
				}
			}
		}

		return $output;		
	}
	
	public function valid_request()
	{
		$valid = true;
		$routes = $this->routes;
		$output = false;
		$request = $this->has_request();
		
		if($request)
		{
			if(property_exists($request, 'request') && property_exists($request, 'query_vars'))
			{	
				foreach( $routes as $k => $v )
				{	
					if( array_key_exists( $k, $request->query_vars ) )
					{
						if( array_key_exists( 'strlen', $routes[$k] ) )
						{
							if(strlen($request->query_vars[$k]) != $routes[$k]['strlen'] )
							{
								$valid = false;
							}
						}
						if( array_key_exists( 'regex', $routes[$k] ) )
						{
							if( !preg_match( $routes[$k]['regex'], $request->query_vars[$k] ) )
							{
								$valid = false;
							}
						}
					}
					else
					{
						if(array_key_exists( 'add_lang', $routes[$k]))
						{
							foreach($routes[$k]['add_lang'] as $k2 => $v2)
							{	
								if(array_key_exists($v2, $request->query_vars))
								{
									if(array_key_exists( 'strlen', $routes[$k]))
									{
										if( strlen($request->query_vars[$v2]) != $routes[$k]['strlen'] )
										{
											$valid = false;
										}
									}
									if(array_key_exists( 'regex', $routes[$k]))
									{
										if( !preg_match( $routes[$k]['regex'], $request->query_vars[$v2] ) )
										{
											$valid = false;
										}
									}									
								}	
							}
							
						}
					}
				}
				
				$output = ($valid === true) ? $request->request : false;
			}
		}

		return $output;		
	}

	public function template($template)
	{
		$routes = $this->routes;
		
		foreach($routes as $k => $v)
		{
			if(array_key_exists( 'template', $routes[$k]))
			{				
				if($this->get_query_obj($k))
				{
					$new_template = locate_template( array( $routes[$k]['template'] ) );
					
					if($new_template != null)
					{
						$template = $new_template;
					}					
				}	
			}			
		}
		
		return $template;
	}
	
	
	public function rewrite()
	{
		$routes = $this->routes;	
		$add_lang = array();		
		
		foreach($routes as $k => $v)
		{
			$regex1 = strval( '^'.$k.'/([^/]*)/?' );
			$match1 = strval( 'index.php?'.$k.'=$matches[1]' );
			add_rewrite_rule($regex1, $match1,'top' );
			
			if(array_key_exists( 'add_lang', $routes[$k]))
			{
				foreach($routes[$k]['add_lang'] as $k2 => $v2)
				{
					$regex2 = strval( '^'.$k2.'/'.$v2.'/([^/]*)/?' );					
					$match2 = strval( 'index.php?'.$v2.'=$matches[1]' );					
					add_rewrite_rule($regex2, $match2,'top' );				
				}
			}
		}			
	}

	public function tags()
	{
		$routes = $this->routes;

		foreach($routes as $k => $v)
		{
			$str = strval( '%'.$k.'%' );
			add_rewrite_tag($str, '([^&]+)' );
			
			if(array_key_exists('add_lang', $routes[$k]))
			{
				foreach($routes[$k]['add_lang'] as $k2 => $v2)
				{	
					$str2 = strval( '%'.$v2.'%' );
					add_rewrite_tag($str2, '([^&]+)', $k.'=' );				
				}				
			}
		}
	}
	
	public function query_vars($query_vars)
	{
		$routes = $this->routes;
		
		foreach($routes as $k => $v)
		{
			$query_vars[] = $k;
			
			if(array_key_exists('add_lang', $routes[$k]))
			{
				foreach($routes[$k]['add_lang'] as $k2 => $v2)
				{
					 $query_vars[] = $k2;
					 $query_vars[] = $v2;
				}				
			}
		}
		
		return array_unique($query_vars);
	}
	
	public function post_link($permalink, $post, $leavename)
	{		
		$routes = $this->routes;
		
		foreach($routes as $k => $v)
		{
			if($this->get_query_obj($k) && $this->valid_request())
			{
				$permalink = esc_url_raw(get_home_url() .'/'. $this->valid_request());				
			}
		}
		
		return $permalink;
	}
}