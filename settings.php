<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Settings
{
	public function __construct()
	{	
		$this->top_bar = get_option('woohacked_top_bar');
		$this->breadcrumb = get_option('woohacked_breadcrumb');
		$this->contact = get_option('woohacked_contact');
		$this->pickup_instructions = get_option('woohacked_pickup_instructions');
		$this->package_description = get_option('woohacked_package_description');
		$this->telephone = get_option('woohacked_telephone');
		$this->whatsapp = get_option('woohacked_whatsapp');
		$this->init();
	}
	public function init()
	{
		add_filter( 'woocommerce_general_settings', array(&$this, 'top_bar'));
		add_filter( 'woocommerce_catalog_orderby', array(&$this, 'remove_sorting_options'));
		add_filter('woocommerce_default_catalog_orderby', array(&$this, 'set_default_sorting'));
	}
	public function set_default_sorting($sort_by)
	{
		return 'date';
	}
	public function remove_sorting_options($options)
	{
		unset( $options[ 'popularity' ] );
		unset( $options[ 'menu_order' ] );
		unset( $options[ 'rating' ] );
		//unset( $options[ 'date' ] );
		//unset( $options[ 'price' ] );
		//unset( $options[ 'price-desc' ] );
	 
		return $options;	
	}
	public function top_bar($settings)
	{
		$settings[] =  array(
          'title' => __( 'Site Notifications', 'woohacked' ),
          'type'  => 'title',
          'id'    => 'woohacked',
        );
		
		$settings[] = array(
			'title' => __('Front Top Bar', 'woohacked'),
			'type' => 'textarea',
			'id' => 'woohacked_top_bar'
		);
		
		$settings[] = array(
			'title' => __('Breadcrumb', 'woohacked'),
			'type' => 'textarea',
			'id' => 'woohacked_breadcrumb'
		);		
		
		$settings[] = array(
			'title' => __('Contact Person', 'woohacked'),
			'type' => 'text',
			'id' => 'woohacked_contact'
		);		
		
		$settings[] = array(
			'title' => __('Telephone', 'woohacked'),
			'type' => 'text',
			'id' => 'woohacked_telephone'
		);			

		$settings[] = array(
			'title' => __('Whatsapp', 'woohacked'),
			'type' => 'text',
			'id' => 'woohacked_whatsapp'
		);
		$settings[] = array(
			'title' => __('Pickup Instructions', 'woohacked'),
			'type' => 'textarea',
			'id' => 'woohacked_pickup_instructions'
		);		
		
		$settings[] = array(
			'title' => __('Package Description', 'woohacked'),
			'type' => 'textarea',
			'id' => 'woohacked_package_description'
		);			
		
		$settings[] =  array(
          'type'  => 'sectionend',
          'desc'  => '',
          'id'    => 'woohacked',
        );		
		
		return $settings;		
	}	
}