<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_API
{
	function __construct($plugin_name, $api_endpoint)
	{
		$this->plugin_name = $plugin_name;
		$this->api_endpoint = $api_endpoint;
		$this->init();
	}

	public function init()
	{
		add_action('wp_headers', array(&$this, 'init_wc_ajax'), 1);
		add_action('parse_query', array(&$this, 'store_config'), 1);
		add_action( 'wp', array(&$this, 'store_config_step_2'), 1 );
		add_filter( 'woocommerce_shipping_settings', array(&$this, 'woohacked_options'));
	}
	public function init_wc_ajax($headers)
	{
		global $WOOHACKED_STORE_CONFIG; 
		
		if(!isset($WOOHACKED_STORE_CONFIG))
		{
			if(isset($_GET['wc-ajax']))
			{
				if($_GET['wc-ajax'] == 'update_order_review')
				{
					$GLOBALS['WOOHACKED_STORE_CONFIG'] = $this->get_store_config();
				}
			}			
		}
	
		return $headers;
	}
	public function store_config()
	{
		global $WOOHACKED_STORE_CONFIG; 
		global $wp;
		$valid = false;
		
		if(!isset($WOOHACKED_STORE_CONFIG))
		{
			if(isset($wp))
			{
				if(property_exists($wp, 'request') && property_exists($wp, 'query_vars'))
				{
					
					if($wp->request == get_post_field('post_name', get_option('woocommerce_cart_page_id')))
					{
						$valid = true;
					}
					
					else if($wp->request == get_post_field('post_name', get_option('woocommerce_checkout_page_id')))
					{
						$valid = true;
					}
				}
			}	
					
			if($valid)
			{
				$GLOBALS['WOOHACKED_STORE_CONFIG'] = $this->get_store_config();
			}			
		}
		
	}
	public function store_config_step_2($query)
	{
		global $WOOHACKED_STORE_CONFIG;
		
		if(!isset($WOOHACKED_STORE_CONFIG))
		{
			if(is_cart() || is_checkout())
			{
				$GLOBALS['WOOHACKED_STORE_CONFIG'] = $this->get_store_config();
			}			
		}
	}
	public function get_store_config()
	{
		$output = null;
		global $WOOHACKED_STORE_CONFIG_GET;
		
		if(isset($WOOHACKED_STORE_CONFIG_GET))
		{
			$output = $WOOHACKED_STORE_CONFIG_GET;
		}
		else
		{
			if(get_option('woohacked_store_id') && get_option('woohacked_token'))
			{
				
				
				$url = $this->api_endpoint.'stores/'.get_option('woohacked_store_id').'/'.get_option('woohacked_token');
				$args = array();
				
				if($_SERVER['HTTP_HOST'] != 'localhost')
				{
					$args['Origin'] = $_SERVER['SERVER_NAME'];
				}
				
				$response = wp_remote_get($url, $args);
				
				//wp_die(json_encode($response));
							
				if(is_array($response))
				{
					if(array_key_exists('body', $response))
					{
						$body = json_decode($response['body'], true);
						
						if(is_array($body))
						{
							if(array_key_exists('code', $body))
							{
								if($body['code'] == 200)
								{
									$output = $body['data'];
									$GLOBALS['WOOHACKED_STORE_CONFIG_GET'] = $output;
								}
							}
						}
					}
				}
			}			
		}
		
		return $output;
	}
	
	public function woohacked_options($settings)
	{
		$settings[] =  array(
          'title' => $this->plugin_name,
          'type'  => 'title',
          'id'    => 'woohacked',
        );
		
		$settings[] = array(
			'title' => 'Store ID',
			'type' 	=> 'text',
			'id'    => 'woohacked_store_id',
		);
		
		$settings[] = array(
			'title' => 'Store Token',
			'type' => 'text',
			'id'    => 'woohacked_token',
		);

		$settings[] =  array(
          'type'  => 'sectionend',
          'id'    => 'woohacked',
        );
		
		return $settings;
	}	
	
}