<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Storefront_Tricks
{
	
	public function __construct($settings)
	{
		$this->top_bar = $settings->top_bar;
		$this->breadcrumb = $settings->breadcrumb;
		$this->whatsapp = $settings->whatsapp;
		$this->init();
	}
	public function init()
	{
		add_action( 'storefront_before_header', array(&$this, 'topbar') );
		add_action( 'woocommerce_after_add_to_cart_form', array(&$this, 'share_links') );
		add_action( 'woohacked_share_links', array(&$this, 'whatsapp_share_cb') );
		add_action( 'woohacked_share_links', array(&$this, 'facebook_share_cb') );
		add_action( 'woohacked_share_links', array(&$this, 'twitter_share_cb') );
		add_action( 'woocommerce_before_shop_loop', array(&$this, 'breadcrumb'), 1 );
		add_shortcode( 'whatsapp', array(&$this, 'whatsapp_support'));
		add_shortcode( 'email', array(&$this, 'email_obfuscate'));	
	}

	public function email_obfuscate($email, $content = '')
	{
		if(is_array($email))
		{
			if(array_key_exists('email', $email))
			{
				$email = $email['email'];
				
				if(is_email($email))
				{
					$character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
					$key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);
					for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
					$script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
					$script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
					$script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
					$script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")"; 
					$script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';
					return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;		
				}
			}
			if(array_key_exists('text', $email) && array_key_exists('url', $email))
			{
				$link = base64_encode('<a href="'.esc_url($email['url']).'" targe="_blank">'.esc_html($email['text']).'</a>');
				$script = '<script type="text/javascript">document.write(atob("'.$link.'"));</script>';	
				return $script;
			}
		}
	}
	
	public function whatsapp_support()
	{
		if($this->whatsapp != '')
		{
			return '<a class="button whatsapp" target="_blank" href="https://wa.me/'.esc_html($this->whatsapp).'">Whatsapp</a>';
		}
	}
	public function price_label()
	{
		global $post;
		$product = wc_get_product( $post->ID );
		$output = null;
		
		if($product)
		{
			$price = html_entity_decode(get_woocommerce_currency_symbol()) . $product->get_price();
			$output = __('from', 'woohacked') . ' ' . $price;			
		}
		
		return $output;
	}
	public function twitter_share_cb()
	{
		echo $this->twitter_share();
	}
	public function twitter_share()
	{
		$title = get_the_title();
		$price = $this->price_label();
		$text = ($price) ? "${title} ${price}" : $title;
		return '<a class="button twitter" target="_blank" href="http://twitter.com/share?url='.urlencode(get_the_permalink()).'&text='.urlencode($text).'">Twitter</a>';
	}	
	public function facebook_share_cb()
	{
		echo $this->facebook_share();
	}
	public function facebook_share()
	{
		return '<a class="button facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.urlencode(get_the_permalink()).'">Facebook</a>';
	}
	
	public function whatsapp_share_cb()
	{
		echo $this->whatsapp_share();
	}	
	public function whatsapp_share()
	{
		$title = get_the_title();
		$price = $this->price_label();
		$url = get_the_permalink();
		$text = ($price) ? "${title} ${price} ${url}" : "${title} ${url}";
		return '<a class="button whatsapp" target="_blank" href="https://wa.me/?text='.urlencode($text).'">Whatsapp</a>';
	}
	public function share_links()
	{
		echo '<hr/><div class="woohacked-share-links">';
		echo '<div><em>'.esc_html(__('Share in', 'woohacked')).'</em>:</div>';
		do_action('woohacked_share_links');
		echo '</div><hr/>';
	}
	
	public function breadcrumb()
	{
		if($this->breadcrumb != '' && is_shop()) {
			?>
			<div id="woohacked_shop_header" style="width: 450px; max-width: 100%; margin: 0 auto 40px auto;">
				<div class="col-full">
					<?php echo stripslashes(wp_filter_post_kses($this->breadcrumb)); ?>
				</div>
			</div>
			<?php
		}		
	}
	public function topbar() 
	{
		if($this->top_bar != '') {
			?>
			<div id="topbar">
				<div class="col-full">
					<?php echo stripslashes(wp_filter_post_kses($this->top_bar)); ?>
				</div>
			</div>
			<?php
		}
	}
}