<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Credits
{
	public function __construct($plugin_name, $plugin_url, $plugin_author, $plugin_author_url)
	{
		$this->plugin_name = $plugin_name;
		$this->plugin_url = $plugin_url;
		$this->plugin_author = $plugin_author;
		$this->plugin_author_url = $plugin_author_url;	
		$this->delivery_providers = implode(', ', array('Glovo', 'Multientrega'));
		$this->router = new WP_Dynamic_Router($this->routes());
		$this->init();
	}
	public function init()
	{
		add_filter( 'pre_get_document_title', array( &$this, 'wp_title' ), 99);
		add_filter( 'wp_title', array( &$this, 'wp_title' ), 99);
		add_filter( 'the_title', array( &$this, 'the_title' ), 99);
		add_filter( 'the_content', array( &$this, 'the_content' ), 99);
		add_filter( 'get_the_excerpt', array( &$this, 'get_the_excerpt' ), 99);
		add_action( 'pre_get_posts', array( &$this, 'set_post_type' ), 99);
		add_action( 'parse_query', array( &$this, 'build_api' ), 1);
		add_action('wp_head', array(&$this, 'meta_tags'));
		add_action( 'init', array(&$this, 'modify_template_parts'));
	}
	public function modify_template_parts()
	{
		remove_action( 'storefront_footer', 'storefront_credit', 20 );
		add_action( 'storefront_footer', array(&$this, 'custom_storefront_credit'), 100 );
		add_filter('woocommerce_structured_data_breadcrumblist', array(&$this, 'data_breadcrumb'));
	}

	public function data_breadcrumb($breadcrumb)
	{
		if($this->valid_request())
		{
			return false;
		}
		
		return $breadcrumb;
	}

	public function custom_storefront_credit() {		
		?>
		<div class="site-info woohacked-credits">
			<p>&copy; <?php echo get_bloginfo( 'name' ) . ' ' . get_the_date( 'Y' ); ?></p>
			<p><?php echo esc_html(__('Built with', 'woohacked')); ?> <a href="<?php echo esc_url(get_home_url().'/'.$this->plugin_name.'/e-commerce'); ?>"><?php echo esc_html($this->plugin_name); ?></a></p>
		</div><!-- .site-info -->
		<?php
	}

	public function args()
	{
		$hash1 = (strlen(get_bloginfo('name')) % 2 == 0) ? true: false;
		$hash2 = (strlen(get_bloginfo('url')) % 2 == 0) ? true: false;
		$output = (object) array();
		$output->design = ($hash2) ? __('Design', 'woohacked') : __('development', 'woohacked');
		$output->store_type = ($hash1) ? sprintf(__('Virtual Store %s', 'woohacked'), $output->design) : sprintf(__('Online Shop %s', 'woohacked'), $output->design);
		$output->title = $output->store_type . ' ' . $output->design;
		$output->domain_name = ($hash1) ? strtoupper(get_bloginfo('name')) : strtoupper($_SERVER['SERVER_NAME']);
		$output->country = $this->get_location()->country;
		$output->state = $this->get_location()->state;
		return $output;
	}
	public function routes()
	{
		return array($this->plugin_name => array('regex' => '/^[a-zA-Z\-]/', 'template' => 'page.php'));
	}
	public function build_api()
	{
		if($this->has_request())
		{	
			if(!$this->valid_request())
			{
				$this->page_404();
			}
			else
			{
				if($this->get_query_var($this->plugin_name) == 'e-commerce')
				{
					//do nothing yeat
				}
				else
				{
					$this->page_404();
				}
			}
		}
	}
	public function valid_request()
	{
		return $this->router->valid_request();
	}
	public function has_request()
	{
		return $this->router->has_request();
	}	

	public function get_query_var( $query_var )
	{
		
		$routes = $this->routes();
		$query = $this->router->get_query_obj( $query_var );
		$output = false;
		
		foreach( $routes as $k => $v )
		{
			if( array_key_exists($query_var, $routes) )
			{
				$output = $query;
			}
		}
		
		return $output;
	}
	
	public function set_post_type( $query )
	{
		if( $query->is_main_query() && $this->valid_request() )
		{
			if(  $this->get_query_var( $this->plugin_name ) )
			{
				$query->set('post_type', 'page');
				$query->set( 'posts_per_page', 1 );					
			}			
		}
	}
	
	public function wp_title( $title )
	{
		if( $this->valid_request() )
		{
			
			if( $this->get_query_var( $this->plugin_name ) == 'e-commerce')
			{				
				$title = sprintf(__('E-commerce & %s in %s', 'woohacked'), $this->args()->store_type, $this->args()->country);
				$title = esc_html('🛒 '.$title . ' | ' . get_bloginfo('name'));
			}
		}
		
		
		
		return $title;
	}
	
	public function the_title( $title )
	{
		if( $this->valid_request() && in_the_loop() )
		{			
			if( $this->get_query_var( $this->plugin_name ) == 'e-commerce')
			{
				$title = sprintf(__('E-commerce & %s in %s, %s', 'woohacked'), $this->args()->store_type, $this->args()->state, $this->args()->country);
			}				
		}
		
		return $title;
	}

	public function the_content( $content )
	{
		if( $this->valid_request() )
		{
			
			if( $this->get_query_var( $this->plugin_name ) == 'e-commerce')
			{
				$plugin_name = '<strong>'.$this->plugin_name.'</strong>';
				$plugin_link = '<a href="'.$this->plugin_url.'" target="_blank">'.$plugin_name.'</a>';
				$plugin_author_link = '<a href="'.$this->plugin_author_url.'" target="_blank">'.$this->plugin_author.'</a>';
				$map = function($row){ return '<p>'.$row.'</p>'; };
				$p = array();
				$p[] = sprintf(__('%s is built with Wordpress, Woocommerce and the awesome e-commerce plugin %s, developed by the panamanian web developer %s.', 'woohacked'), $this->args()->domain_name, $plugin_link, $plugin_author_link);
				$p[] = sprintf(__('%s lets any store in %s, %s to integrate %s and many other delivery providers.', 'woohacked'), $plugin_name, $this->args()->state, $this->args()->country, $this->delivery_providers);
				$content = implode('', array_map($map, $p));
			}
		}
		
		return $content;
	}
	
	public function get_the_excerpt( $excerpt )
	{
		if( $this->valid_request() )
		{			
			if( $this->get_query_var( $this->plugin_name ) == 'e-commerce')
			{
				$excerpt = $this->plugin_name;
			}				
		}
		
		return $excerpt;
	}
	
	public function get_location()
	{
		$output = (object) array();
		$raw_country = get_option( 'woocommerce_default_country' );
		$split_country = explode(':', $raw_country);
		
		if(is_array($split_country))
		{
			if(count($split_country) === 2)
			{
				$country_code = $split_country[0];
				$state_code = $split_country[1];
				$output->country = WC()->countries->countries[$country_code];
				$states = WC()->countries->get_states($country_code);
				$output->state = $states[$state_code];
			}
		}

		
		return $output;
	}
	public function page_404()
	{
		$message = '<h1>'.esc_html(__('Page Not Found', 'woohacked')).'</h1>';
		$message .= '<p>'.esc_html(__('The page you are looking for was moved, removed, renamed or might never existed.', 'woohacked')).'</p>';
		$message .= '<p><a class="button button-large" href="'.esc_url(get_home_url()).'">'.esc_html(__('Go Home', 'woohacked')).'</a></p>';	
		wp_die($message, esc_html(__('404', 'woohacked')), array('response' => 404));		
	}
	public function meta_tags()
	{		
		if( $this->valid_request())
		{			
			if( $this->get_query_var( $this->plugin_name ) == 'e-commerce')
			{
				$meta = array();
				$meta[] = sprintf(__('E-commerce & %s in %s, %s', 'woohacked'), $this->args()->store_type, $this->args()->state, $this->args()->country);
				$meta[] = sprintf(__('%s is built with Wordpress, Woocommerce and the awesome e-commerce plugin %s.', 'woohacked'), $this->args()->domain_name, $this->plugin_name);
				$meta[] = sprintf(__('%s lets any store in %s, %s to integrate %s and many other delivery providers.', 'woohacked'), $this->plugin_name, $this->args()->state, $this->args()->country, $this->delivery_providers);				
				
				echo '<meta name="description" content="'.esc_html(implode(' ', $meta)).'">';
			}				
		}		
	}
}