<?php

if (!defined('ABSPATH'))
{
    return;
}

class Woohacked_Checkout
{
	public function __construct($settings, $get_store_config)
	{
		$this->contact = $settings->contact;
		$this->telephone = $settings->telephone;
		$this->whatsapp = $settings->whatsapp;
		$this->pickup_instructions = $settings->pickup_instructions;
		$this->package_description = $settings->package_description;
		$this->get_store_config = $get_store_config;
		$this->init();
	}
	public function init()
	{
		add_filter('woocommerce_states', array($this, 'sell_in_provinces'), 10, 1);
		add_filter('woocommerce_default_address_fields', array($this, 'order_address_fields'), 100, 1);
		add_action('woocommerce_checkout_fields', array($this, 'coordinates_field'), 10, 1);
		add_action('woocommerce_admin_order_data_after_shipping_address', array($this, 'order_fields_admin'), 10, 1);
		add_filter('woohacked_args', array($this, 'json_cities'), 10, 1);
		add_action('woocommerce_order_status_processing', array($this, 'order_processing'));		
	}
	public function order_processing($order_id)
	{
		$which_var = 'order_processing_'.$order_id;
		global $$which_var;
		$WOOHACKED_STORE_CONFIG = $this->get_store_config;
		
		if(!isset($$which_var) && $WOOHACKED_STORE_CONFIG)
		{
			$order = wc_get_order($order_id);
			
			
			if ($order->get_status() == 'processing')
			{
				$shipping_methods = array();
				
				foreach( $order->get_items( 'shipping' ) as $item_id => $shipping_item_obj )
				{
					$method = array();
					$method['item_name'] = $shipping_item_obj->get_name();
					$method['id'] = $shipping_item_obj->get_method_id();
					$method['instance_id'] = $shipping_item_obj->get_instance_id();
					$shipping_methods[] = $method;
				}
				
				if(is_array($shipping_methods))
				{
					$count_methods = count($shipping_methods);
					$true_methods = 0;
					
					if($count_methods > 0)
					{
						for($x = 0; $x < $count_methods; $x++)
						{
							if($shipping_methods[$x]['id'] == 'Woohacked_Glovo')
							{
								$true_methods++;
							}
						}
					}
					
					if($true_methods == $count_methods)
					{
						//fix here
						$delivery_woohacked_client_coordinates = get_post_meta($order_id, '_shipping_woohacked_client_coordinates', true);
						
						if($delivery_woohacked_client_coordinates != '')
						{
							$new_delivery_woohacked_client_coordinates = explode(',', $delivery_woohacked_client_coordinates);
							
							if(is_array($new_delivery_woohacked_client_coordinates))
							{
								if(count($new_delivery_woohacked_client_coordinates) == 2)
								{
									$client_latitude = $new_delivery_woohacked_client_coordinates[0];
									$client_longitude = $new_delivery_woohacked_client_coordinates[1];									
								}
							}
						}
						
						if(isset($client_latitude) && isset($client_longitude))
						{	
							$payload = array();
							$payload['scheduleTime'] = null;
							$description = '#' . $order_id . ': ' . $this->package_description;
							$payload['description'] = esc_html(substr($description, 0, 255));
							$payload['reference'] = array('id' => $order_id);
							$payload['addresses'] = array();
							$pickup = array();
							$pickup['type'] = 'PICKUP';
							$pickup['lat'] = floatval($WOOHACKED_STORE_CONFIG['coordinates']['_latitude']);
							$pickup['lon'] = floatval($WOOHACKED_STORE_CONFIG['coordinates']['_longitude']);
							$pickup['label'] = esc_html(substr(get_option('woocommerce_store_address'), 0, 255));
							$pickup['details'] = esc_html(substr(get_option('woocommerce_store_address_2'), 0, 255));
							$store_phone = $this->format_phone($this->telephone);
							
							if($store_phone != '')
							{
								$pickup['contactPhone'] = $this->format_phone($this->telephone);
							}
							
							$pickup['contactPerson'] = esc_html(substr($this->contact . ' ('. get_bloginfo('name') . ')', 0, 255));
							
							$pickup_instructions = $this->pickup_instructions;
							
							if($pickup_instructions == '')
							{
								if($store_phone != '')
								{
									$pickup_instructions = $store_phone;
								}
							}
							
							$pickup['instructions'] = esc_html(substr($pickup_instructions,0, 255));
							$payload['addresses'][] = $pickup;
							
							$delivery = array();
							$delivery['type'] = 'DELIVERY';
							$delivery['lat'] = floatval($client_latitude);
							$delivery['lon'] = floatval($client_longitude);
							$delivery['label'] = esc_html(substr($order->get_shipping_address_1(), 0, 255));
							$delivery['details'] = esc_html(substr($order->get_shipping_address_2(), 0, 255));
							
							$customer_phone = $this->format_phone($order->get_billing_phone());
							
							if($customer_phone != '')
							{
								$delivery['contactPhone'] = $customer_phone;
							}
							
							$customer = $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name();
							
							if($order->get_shipping_company() != '')
							{
								$customer .= ' (' . $order->get_shipping_company() . ')';
							}
							
							$delivery['contactPerson'] = esc_html(substr($customer, 0, 255));
							$delivery_instructions = $order->get_customer_note();
							
							if($delivery_instructions == '')
							{
								if($customer_phone != '')
								{
									$delivery_instructions = $customer_phone;
								}
								else
								{
									$delivery_instructions = $order->get_billing_phone();
								}
							}
							
							$delivery['instructions'] = esc_html(substr($delivery_instructions, 0, 255));					
							$payload['addresses'][] = $delivery;
							$url = 'https://api.glovoapp.com';
							$endpoint = '/b2b/orders';
							$authorization = base64_encode($WOOHACKED_STORE_CONFIG['glovo_api_key'] . ':' . $WOOHACKED_STORE_CONFIG['glovo_secret_key']);
							$headers = array('Authorization: Basic ' . $authorization, 'Content-Type: application/json');

							$ch = curl_init($url . $endpoint);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
							curl_setopt($ch, CURLOPT_TIMEOUT, 10);
							curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$response = curl_exec($ch);
							curl_close($ch);
														
							$data = json_decode($response, true);
							
							if(is_array($data))
							{
								if(array_key_exists('id', $data) && array_key_exists('code', $data))
								{
									add_post_meta($order_id, '_shipping_woohacked_glovo_id', $data['id']);
									add_post_meta($order_id, '_shipping_woohacked_glovo_code', $data['code']);
								}
							}
						}					
					}
				}
			}			
		}
	}
	
	public function format_phone($phone)
	{
		$output = '';
		
		if($phone != '')
		{
			$number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
			
			if($number >= 2000000 && $number <= 99999999)
			{
				$output = '+507' . $number;
			}
			else if($number >= 5070000000 && $number <= 50799999999)
			{
				$output = '+' . $number;
			}			
		}
		
		return $output;
	}
	
	public function order_fields_admin($order)
	{
		$order_id = $order->get_id();
		$coordinates = get_post_meta( $order_id, '_shipping_woohacked_client_coordinates', true );
		$glovo_id = get_post_meta( $order_id, '_shipping_woohacked_glovo_id', true );
		$glovo_code = get_post_meta( $order_id, '_shipping_woohacked_glovo_code', true );
		
		echo '<p class="form-field _shipping_woohacked_client_coordinates"><label for="_shipping_woohacked_client_coordinates">'.__('Coordinates', 'woohacked').':</label> <a target="_blank" href="https://www.google.com/maps/@'.esc_html($coordinates).',19.18z">'.esc_html($coordinates).'</a></p>';
		
		echo '<p class="form-field _shipping_woohacked_glovo_id"><label for="_shipping_woohacked_glovo_id">'.__('Glovo Order ID', 'woohacked').':</label>'.esc_html($glovo_id).'</p>';
		
		echo '<p class="form-field _shipping_woohacked_glovo_code"><label for="_shipping_woohacked_glovo_code">'.__('Glovo Order Code', 'woohacked').':</label>'.esc_html($glovo_code).'</p>';
	}
	public function coordinates_field($fields)
	{
		$billing_class = array('woohacked-coordinates-field');
		$shipping_class = array('woohacked-coordinates-field');

		$fields['order']['has_no_coordinates'] = array(
			'type' => 'text',
			'required' => false
		);
		
		$fields['billing']['billing_woohacked_client_coordinates'] = array(
			'type' => 'text',
			'required' => true,
			'class' => $billing_class,
			'clear' => true,
			'priority' => 39,
			'label' => __('Find the exact address on the map', 'woohacked')
		);

		$fields['shipping']['shipping_woohacked_client_coordinates'] = array(
			'type' => 'text',
			'required' => true,
			'class' => $shipping_class,
			'clear' => true,
			'priority' => 39,
			'label' =>  __('Find the exact address on the map', 'woohacked')		
		);
		
		//
		$fields['billing']['billing_first_name']['priority'] = 91;
		$fields['billing']['billing_last_name']['priority'] = 92;
		$fields['shipping']['shipping_first_name']['priority'] = 91;
		$fields['shipping']['shipping_last_name']['priority'] = 92;		
		
		return $fields;
	}	
	public function json_cities($args)
	{
		$data = $this->cities();
		$output = array();
		
		foreach($data as $states_keys => $cities)
		{
			if(!array_key_exists($states_keys, $output))
			{
				$output[$states_keys] = array();
			}
				
			for($c = 0; $c < count($cities); $c++)
			{
				array_push($output[$states_keys], $cities[$c]);
			}
		}
		
		$args['cities'] = $output;
		
		return $args;
	}
	public function cities()
	{
		$cities = array(

			'PA' => array(
				'24 de Diciembre',
				'Alcalde Díaz',
				'Ancón',
				'Betania',
				'Bella Vista',
				'Calidonia',
				'Caimitillo',
				'Chilibre',
				'El Chorrillo',
				'Curundú',
				'Ernesto Córdoba Campos',
				'Juan Díaz',
				'Las Cumbres',
				'Las Mañanitas',
				'Pacora',
				'Parque Lefevre',
				'Pedregal',
				'Pueblo Nuevo',
				'Río Abajo',
				'San Felipe',
				'San Francisco',
				'San Martín',
				'Santa Ana',
				'Tocumen'
			),
			'SM' => array(
				'Amelia Denis De Icaza',
				'Belisario Porras',
				'José Domingo Espinar',
				'Mateo Iturralde',
				'Victoriano Lorenzo',
				'Arnulfo Arias',
				'Belisario Frías',
				'Omar Torrijos',
				'Rufina Alfaro'			
			)
		);
		
		return $cities;	
	}
	public function order_address_fields($fields)
	{
		$fields['state']['priority'] = 41;
		$fields['state']['label'] =  __('State / County', 'woohacked');
		$fields['city']['priority'] = 42;
		$fields['city']['label'] = __('City', 'woohacked');	

		$fields['city']['type'] = 'select';			
		$fields['city']['options'] = array('' => '');
		
		unset($fields['company']);				
		unset($fields['postcode']);				
		return $fields;
	}
	public function sell_in_provinces($provinces)
	{		
		$provinces['PA'] = array(

			'PA' => 'Ciudad de Panamá',
			'SM' => 'San Miguelito'
		);
		return $provinces;
	}

}