<?php

if (!defined('ABSPATH'))
{
    return;
}

function Woohacked_Glovo_Method_Init($methods)
{
	$methods['Woohacked_Glovo'] = 'Woohacked_Glovo';
	return $methods;		
}

function Woohacked_Glovo_Init()
{
	class Woohacked_Glovo extends WC_Shipping_Method
	{
		public function __construct( $instance_id = 0 ) {
			$this->id = 'Woohacked_Glovo';
			$this->instance_id = absint( $instance_id );
			$this->method_title = 'Glovo'; 
			$this->title = 'Glovo'; 
			$this->method_description = sprintf(__('Send with %s', 'woohacked'), $this->method_title);
			//$this->countries = array('PA');
			$this->supports = array('shipping-zones', 'instance-settings');
			
			$this->instance_form_fields = array(
				'enabled' => array(
					'title' 		=> sprintf(__('Enable %s in this Zone?', 'woohacked'), $this->method_title) ,
					'type' 			=> 'checkbox',
					'label' 		=> sprintf(__('Check the box if you want to activate %s in this zone', 'woohacked'), $this->method_title),
					'default' 		=> 'yes',
				),
				'title' => array(
					'title' 		=> sprintf(__('%s title', 'woohacked'), $this->method_title),
					'type' 			=> 'text',
					'description' 	=> sprintf(__('This is how you will call %s in your site', 'woohacked'), $this->method_title),
					'default'		=> $this->method_title
				)			
			);			

			$this->set_client_coordinates();
			$this->init();
			$this->enabled = $this->get_option( 'enabled' );
			$this->title = $this->get_option( 'title' );
		}
		
		public function init() 
		{
			add_action( 'woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
		}
		
		public function set_client_coordinates()
		{
			if(isset($_GET['wc-ajax']))
			{
				if($_GET['wc-ajax'] == 'update_order_review')
				{
					if(isset($_POST['post_data']))
					{
						$post_data = parse_str($_POST['post_data'], $params);
						
						if(array_key_exists('has_no_coordinates', $params))
						{
							$has_no_coordinates = sanitize_text_field($params['has_no_coordinates']);
							$GLOBALS['has_no_coordinates'] = ($has_no_coordinates == 'off') ? 'off' : '';						
						}
						
						if(array_key_exists('billing_woohacked_client_coordinates', $params))
						{
							$woohacked_client_coordinates = sanitize_text_field($params['billing_woohacked_client_coordinates']);
							
							if($woohacked_client_coordinates != '')
							{							
								if(count(explode(',', $woohacked_client_coordinates)) == 2)
								{
									$GLOBALS['woohacked_client_coordinates'] = $woohacked_client_coordinates;
								}
							}
						}
						if(array_key_exists('shipping_woohacked_client_coordinates', $params))
						{
							$woohacked_client_coordinates = sanitize_text_field($params['shipping_woohacked_client_coordinates']);
							
							if($woohacked_client_coordinates != '')
							{
								if(count(explode(',', $woohacked_client_coordinates)) == 2)
								{
									$GLOBALS['woohacked_client_coordinates'] = $woohacked_client_coordinates;
								}
							}
						}
					}				
				}
			}
			else
			{
				if(isset($_POST['woohacked_client_coordinates']) && isset($_POST['has_no_coordinates']))
				{
					$woohacked_client_coordinates = sanitize_text_field($_POST['woohacked_client_coordinates']);
					$has_no_coordinates = sanitize_text_field($_POST['has_no_coordinates']);
					$GLOBALS['has_no_coordinates'] = ($has_no_coordinates == 'off') ? 'off' : '';
						
					if(count(explode(',', $woohacked_client_coordinates)) == 2)
					{
						$GLOBALS['woohacked_client_coordinates'] = $woohacked_client_coordinates;
					}
				}			
			}

		}
	
		public function calculate_shipping( $package = array() )
		{			
			$rate = array(
				'id' => $this->id . $this->instance_id,
				'label' => $this->title,
				'cost' => $this->get_quote($this->id, $this->instance_id),
				'calc_tax' => 'per_item'
			);

			$this->add_rate( $rate );
		}
		public function is_available($package = array())
		{
			$output = false;
			
			if($this->get_quote($this->id, $this->instance_id) > 0)
			{
				$output = true;
			}

			return $output;
		}
		public function get_quote($id, $instance_id)
		{
			global $woohacked_client_coordinates;
			global $WOOHACKED_STORE_CONFIG;
			global $has_no_coordinates;
			$price = 0;
			$min_price = 3.50;
			$which_var = 'get_quote_glovo_'.$instance_id;
			global $$which_var;
						
			if(isset($$which_var))
			{
				$price = $$which_var;
			}
			else
			{				
				if(isset($woohacked_client_coordinates) && isset($WOOHACKED_STORE_CONFIG))
				{
					if($woohacked_client_coordinates != '')
					{
						$new_woohacked_client_coordinates = explode(',', $woohacked_client_coordinates);
						
						if(is_array($new_woohacked_client_coordinates))
						{
							if(count($new_woohacked_client_coordinates) == 2)
							{
								$client_latitude = $new_woohacked_client_coordinates[0];
								$client_longitude = $new_woohacked_client_coordinates[1];
							}							
						}
					}
				}
				
				if(isset($client_latitude) && isset($client_longitude))
				{
					$url = 'https://api.glovoapp.com';
					$endpoint = '/b2b/orders/estimate';
					//$endpoint = '/b2b/working-areas';
					$authorization = base64_encode($WOOHACKED_STORE_CONFIG['glovo_api_key'] . ':' . $WOOHACKED_STORE_CONFIG['glovo_secret_key']);
					
					$store = array('lat' => floatval($WOOHACKED_STORE_CONFIG['coordinates']['_latitude']), 'lon' => floatval($WOOHACKED_STORE_CONFIG['coordinates']['_longitude']));
					
					$client = array('lat' => floatval($client_latitude), 'lon' => floatval($client_longitude));
					$headers = array('Authorization: Basic ' . $authorization, 'Content-Type: application/json');

					$payload = array();
					$payload['description'] = 'descripción de la orden';
					$payload['addresses'] = array();
					
					$pickup = array(
						'type' => 'PICKUP',
						'lat' => $store['lat'],
						'lon' => $store['lon'],
						'label' => $id . ' store'
					);

					$delivery = array(
						'type' => 'DELIVERY',
						'lat' => $client['lat'],
						'lon' => $client['lon'],			
						'label' => $id . ' customer'	
					);
					
					array_push($payload['addresses'], $pickup, $delivery);
																
					$ch = curl_init($url . $endpoint);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$response = curl_exec($ch);
					curl_close($ch);
													
					$response = json_decode($response, true);
									
					if($response != '')
					{
						if(array_key_exists('total', $response))
						{
							if(array_key_exists('amount', $response['total']))
							{						
								if($response['total']['amount'] > 0)
								{
									$price = floatval($response['total']['amount']);
									
									if($WOOHACKED_STORE_CONFIG['shipping_commission'] > 0)
									{
										$shipping_commission = (100 - floatval($WOOHACKED_STORE_CONFIG['shipping_commission'])) / 100;
										$price = $price / $shipping_commission;
									}
									
									$price = round(($price / 100), 2);
								}
								else
								{
									$price = $min_price;
								}

								$GLOBALS[$which_var] = $price;
							}
						}					
					}
				}				
			}
						
			return $price;
		}
	}	
}
