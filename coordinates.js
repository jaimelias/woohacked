"use strict";

jQuery(() => {
	latlonFields().forEach(field => {
		jQuery(field).on('click focus', () => {
			woohacked_modal();
		});		
	});
	
	woohacked_cities();
	woohacked_reset();
	woohacked_validate_calculate();
});

const stateFields = () => {
	return [jQuery('select#billing_state'), jQuery('select#shipping_state'), jQuery('select#calc_shipping_state')].filter(i => i !== 'undefined');
}
const addressFields = () => {
	return [jQuery('#billing_state'), jQuery('#billing_city'), jQuery('#shipping_state'), jQuery('#shipping_city')].filter(i => i !== 'undefined');
}
const latlonFields = () => {
	return [jQuery('input#billing_woohacked_client_coordinates'), jQuery('input#shipping_woohacked_client_coordinates'), jQuery('input#woohacked_client_coordinates')].filter(i => i !== 'undefined');
}
const woohacked_validate_calculate = () => {
	addressFields().forEach(field => {
			
		jQuery(jQuery(field)).change(() => {
			
			woohacked_init_calculator();
			
		});
	});
}

const woohacked_init_calculator = () => {	
	latlonFields().forEach(field => {
		if(jQuery(jQuery(field)).hasClass('confirmed'))
		{
			jQuery( document.body ).trigger( 'init_checkout' );
		}	
	});
}

const woohacked_cities = () => {	
	stateFields().forEach(field => {
		woohacked_options(jQuery(field));
		
		jQuery(field).change(() => {
			woohacked_options(jQuery(field));	
		});
		
	});	
}

const woohacked_options = (this_state) => {
	
	var plugin_args = jQuery('#woohacked-args').text();
	var data = JSON.parse(plugin_args);
	var states_obj = data.cities;
	
	if(typeof jQuery(this_state) !== 'undefined')
	{
		if(jQuery(this_state).val() != '')
		{	
			for(var key in states_obj)
			{	
				if(key == jQuery(this_state).val())
				{
					var options_arr = jQuery('<select><option value=""></option></select>');
			
					states_obj[key].forEach(row => {
						options_arr.append(jQuery('<option></option>').val(row).text(row));
					});
					
					if(jQuery(this_state).attr('id') == 'billing_state')
					{
						jQuery('#billing_city').html(options_arr.find('option'));
					}
					else if(jQuery(this_state).attr('id') == 'shipping_state')
					{
						jQuery('#shipping_city').html(options_arr.find('option'));
					}
					else if(jQuery(this_state).attr('id') == 'calc_shipping_state')
					{
						jQuery('#calc_shipping_city').html(options_arr.find('option'));
					}				
				}
			}
		}		
	}
}

const woohacked_calc_shipping = () => {
	var required_field = jQuery('input#woohacked_client_coordinates');
	
	if(jQuery(required_field).val() == '')
	{
		jQuery(required_field).addClass('invalid');
	}
	else
	{
		jQuery('.woocommerce-shipping-calculator').submit();
	}
}
const woohacked_modal_show = () => {
	jQuery('#woohacked-modal').removeClass('hidden');
	jQuery('#woohacked-overlay').removeClass('hidden');
	jQuery('.storefront-handheld-footer-bar').addClass('hidden');
	jQuery('#woohacked-search').focus();
	
	jQuery('#woohacked-search').on('focus', () => {
		jQuery(this).val('');
	});
}
const woohacked_modal = () => {
	woohacked_modal_show();
	
	jQuery('#woohacked-modal').find('button.modal-btn').click(() => {
		
		//bodal
		jQuery('#woohacked-modal').addClass('hidden');
		jQuery('#woohacked-overlay').addClass('hidden');
		jQuery('.storefront-handheld-footer-bar').removeClass('hidden');
		
		var shipping_woohacked_client_coordinates = getShippingCookie('shipping_woohacked_client_coordinates');
		var shipping_country_code = getShippingCookie('shipping_country_code');

		if(shipping_woohacked_client_coordinates != '')
		{
			if(shipping_country_code.length == 2)
			{
				if(jQuery('input#calc_shipping_country').is(':hidden'))
				{
					jQuery('input#calc_shipping_country').val(shipping_country_code);
				}									
			}
			
			latlonFields().forEach(field => {
				jQuery(field).val(shipping_woohacked_client_coordinates).addClass('confirmed').removeClass('invalid');					
			});
			
			woohacked_reset();
			woohacked_init_calculator();
		}		
	});
	
	var closeTrigger = jQuery('#woohacked-modal').find('.close');
	jQuery(closeTrigger).add('#woohacked-overlay');
	
	jQuery(closeTrigger).click(() => {
		woohacked_modal_hide();
		clearLatLon();
	});
	
	jQuery(document).keyup(e => {
		if (e.keyCode === 27)
		{
			woohacked_modal_hide();
			clearLatLon();
		}
	});	
}

const woohacked_modal_hide = () => {
	modalBtnEnable(false);
	jQuery('#woohacked-modal').addClass('hidden');
	jQuery('#woohacked-overlay').addClass('hidden');
	jQuery('.storefront-handheld-footer-bar').removeClass('hidden');	
}

const woohacked_reset = () => {	

	latlonFields().forEach(field => {

		var this_id = jQuery(jQuery(field)).attr('id');
		
		var selector = 'calc_shipping_';
		
		if(this_id == 'billing_woohacked_client_coordinates')
		{
			selector = 'billing_';
		}
		else if(this_id == 'shipping_woohacked_client_coordinates')
		{
			selector = 'shipping_';
		}
		
		var city = jQuery('#'+selector+'city');
		var state = jQuery('#'+selector+'state');
		var country = jQuery('#'+selector+'country');

		var addressFields = [jQuery(city), jQuery(state), jQuery(country)].filter(i => i !== 'undefined');
		
		addressFields.forEach(addressF => {
			
			if(jQuery(addressF).prop('tagName') == 'SELECT')
			{
				if(!jQuery(addressF).attr('disabled'))
				{
					jQuery(addressF).find('option:first').prop('selected', true);
					jQuery(addressF).select2();								
				}
			}
		});
	});		
	
}

const clearLatLon = () => {
	deleteShippingCookie('shipping_woohacked_client_coordinates');
	
	latlonFields().forEach(field => {
		jQuery(field).removeClass('confirmed').val('');
	});
}

const deleteShippingCookie = (name) => {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
const setShippingCookie = (cname, cvalue, exdays) => {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
const getShippingCookie = (cname) => {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
		  c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
		  return c.substring(name.length, c.length);
		}
	}
	return "";	
}

const modalBtnEnable = (enable) => {
	var btn = jQuery('#woohacked-modal').find('button.modal-btn');
	
	if(enable === true)
	{
		jQuery(btn).prop('disabled', false);
	}
	else
	{
		jQuery(btn).prop('disabled', true);
	}
}

function initMap()
{
	jQuery(() => {
		
		
		const getLocation = () => {
			
			var onSuccess = (position) => {
					var lat = position.coords.latitude;
					var lon = position.coords.longitude;
					showMap(lat, lon);						
				};
			var onError = () => {
					var panamaLat = 8.982379;
					var panamaLon = -79.51987;
					showMap(panamaLat, panamaLon);					
				};
			
			if ('geolocation' in navigator)
			{
				navigator.geolocation.getCurrentPosition(onSuccess, onError);
			}
			else
			{
				onError();
			}
		}		

		const showModal = () => {
			var show = getShippingCookie('shipping_modal');
			
			if(show == 'show')
			{
				getLocation();
			}
			else
			{
				jQuery('#woohacked-share-location').click(() => {
					getLocation();
				});				
			}			
		}
		
		clearLatLon();
		showModal();
		

		const showMap = (lat, lon) => {
			saveLatLon(lat, lon);
			createMap(lat, lon);
			setShippingCookie('shipping_modal', 'show');
			jQuery('#woohacked-modal').find('.woohacked-coordinates-field').removeClass('hidden');			
		}
		const saveLatLon = (lat, lon) => {
			if(lat != '' && lon != '')
			{
				setShippingCookie('shipping_woohacked_client_coordinates', String(lat + ',' + lon), 1);
				
				var plugin_args = JSON.parse(jQuery('#woohacked-args').text());
				var key = plugin_args.gmk;

				var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lon+'&sensor=false&key=' + key;

				jQuery.getJSON(url, data => {
					if(data.hasOwnProperty('results'))
					{
						var results = data.results;
						
						results.forEach(row => {
							
							if(row.types.includes('country'))
							{
								if(row.hasOwnProperty('address_components'))
								{
									var addresses = row.address_components;
									
									addresses.forEach(address => {
										if(address.hasOwnProperty('short_name'))
										{
											if(address.short_name.length == 2)
											{
												setShippingCookie('shipping_country_code', address.short_name, 1);
											}
										}
									});									
								}
							}
						});
					}
				});
				
			}
		}

		const createMap = async (lat, lon) => {
			var plugin_args = jQuery('#woohacked-args').text();
			var input = document.getElementById('woohacked-search');
			
			
			if(plugin_args)
			{
				plugin_args = JSON.parse(plugin_args);			
				var container = document.getElementById('woohacked-map');
				var uluru = {lat: lat, lng: lon};
				var map = new google.maps.Map(container, {zoom: 16, center: uluru, styles: mapStyle()});
				
				var markerIcon = (file_name) => {
					return plugin_args.dir_url + 'assets/' + file_name;
				};
				var marker = new google.maps.Marker({position: uluru, map: map, draggable: true, icon: markerIcon('marker-yellow.svg')});
				let polygons = await polygonsEncoded();
				
				if(polygons)
				{
					var polygonsDecoded = [];
					
					var polyOptions = new google.maps.Polygon({
						strokeColor: '#ADFF2F',
						strokeOpacity: 0.8,
						strokeWeight: 1,
						fillColor: '#ADFF2F',
						fillOpacity: 0.05
					});				
									
					polygons.forEach(row => {
						
						if(row != null)
						{						
							var decoded = google.maps.geometry.encoding.decodePath(row);
							polygonsDecoded.push(decoded);
						}
						
					});
					
					polyOptions.setPaths(polygonsDecoded);
					polyOptions.setMap(map);
					
					if(google.maps.geometry.poly.containsLocation(marker.getPosition(), polyOptions))
					{
						marker.setIcon(markerIcon('marker-yellow.svg'));
					}

					polyOptions.addListener('click', e => {
						
						jQuery(input).blur();
						
						saveLatLon(e.latLng.lat(), e.latLng.lng());
						marker.setPosition(e.latLng);
						modalBtnEnable(true);
						jQuery('input#woohacked-search').val('');				
						
						if(google.maps.geometry.poly.containsLocation(e.latLng, polyOptions))
						{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));					
						}
					});
					
				}
				
				map.addListener('click', e => {
					
					jQuery(input).blur();
					
					saveLatLon(e.latLng.lat(), e.latLng.lng());
					marker.setPosition(e.latLng);
					modalBtnEnable(true);
					jQuery('input#woohacked-search').val('');				
			
					jQuery('input#has_no_coordinates').val('off');
					
					if(polygons)
					{
						jQuery('input#has_no_coordinates').val('off');
						marker.setIcon(markerIcon('marker-red.svg'));
					}
					else
					{
						jQuery('input#has_no_coordinates').val('');
						marker.setIcon(markerIcon('marker-yellow.svg'));
					}
					
				});			

				marker.addListener('dragend', e => {
					
					jQuery(input).blur();
					saveLatLon(e.latLng.lat(), e.latLng.lng());
					modalBtnEnable(true);
					jQuery('input#woohacked-search').val('');				
					
					if(polygons)
					{
						if(google.maps.geometry.poly.containsLocation(e.latLng, polyOptions))
						{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));
						}
						else
						{
							jQuery('input#has_no_coordinates').val('off');
							marker.setIcon(markerIcon('marker-red.svg'));
						}						
					}
					else
					{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));						
					}
				});	

				marker.addListener('click', e => {
					
					jQuery(input).blur();
					saveLatLon(e.latLng.lat(), e.latLng.lng());
					modalBtnEnable(true);
					jQuery('input#woohacked-search').val('');				
	

					if(polygons)
					{
						if(google.maps.geometry.poly.containsLocation(e.latLng, polyOptions))
						{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));
						}
						else
						{
							jQuery('input#has_no_coordinates').val('off');
							marker.setIcon(markerIcon('marker-red.svg'));
						}						
					}
					else
					{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));						
					}					
				});
				
				jQuery(input).on('keyup keypress', e => {
				  var keyCode = e.keyCode || e.which;
				  if (keyCode === 13) { 
					e.preventDefault();
					return false;
				  }
				});			
				
				var panamaCountryCode = 'pa';
				var options = {
				  componentRestrictions: {country: panamaCountryCode}
				};			
				
				
				var autocomplete = new google.maps.places.Autocomplete(input, options);
				
				autocomplete.addListener('place_changed', () =>	{
					var place = autocomplete.getPlace();
					marker.setPosition(place.geometry.location);
					map.setCenter(place.geometry.location);
					map.setZoom(16);
					modalBtnEnable(true);
					saveLatLon(place.geometry.location.lat(), place.geometry.location.lng());				

					if(polygons)
					{
						if(google.maps.geometry.poly.containsLocation(place.geometry.location, polyOptions))
						{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));
						}
						else
						{
							jQuery('input#has_no_coordinates').val('off');
							marker.setIcon(markerIcon('marker-red.svg'));
						}						
					}
					else
					{
							jQuery('input#has_no_coordinates').val('');
							marker.setIcon(markerIcon('marker-yellow.svg'));						
					}					
				});				
			}
			else
			{
				console.log('Error: args not found');
			}
		}
		
		const polygonsEncoded = async () => {
			
			let output = false;
			let plugin_args = jQuery('#woohacked-args').text();
			plugin_args = JSON.parse(plugin_args);
			
			if(plugin_args.hasOwnProperty('polygons_endpoint'))
			{
				let response = await fetch(plugin_args.polygons_endpoint);
				let json = await response.json();
				if(json)
				{
					if(json.hasOwnProperty('data'))
					{
						if(json.data.hasOwnProperty('polygons'))
						{
							output = json.data.polygons;
						}
						else
						{
							console.log('Error: data.polygons in endpont ' + plugin_args.polygons_endpoint);
						}
					}
					else
					{
						console.log('Error: data in endpont ' + plugin_args.polygons_endpoint);
					}
				}
				
			}
			else
			{
				console.log('Error: polygons enpoint not found');
			}
			
			
			return output;
		};
		
		const mapStyle = () => {
			return [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ];	
		}
	
	});	
	
}


