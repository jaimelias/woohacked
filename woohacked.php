<?php
/**
 *
 * Plugin Name: Woohacked
 * Description: Woohacked
 * Version: 1.0
 * Author: Jaimelías
 * Author URI: https://woohacked.com/
 * Text Domain: woohacked
 * Domain Path: /languages
 */

if (!defined('ABSPATH'))
{
    return;
}

$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );

if ( in_array( 'woocommerce/woocommerce.php',  $active_plugins) )
{
	if ( ! function_exists('write_log')) {
		function write_log ( $log )
		{
			if(is_array($log) || is_object($log))
			{
				$log = json_encode($log);
			}
			error_log($log);
		}
	}
	
	if(! function_exists('generateCallTrace'))
	{
		function generateCallTrace()
		{
			$e = new Exception();
			$trace = explode("\n", $e->getTraceAsString());
			$trace = array_reverse($trace);
			array_shift($trace);
			array_pop($trace);
			$length = count($trace);
			$result = array();
		   
			for ($i = 0; $i < $length; $i++)
			{
				$result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
			}
		   
			return "\t" . implode("\n\t", $result);
		}		
	}

	add_action('init', 'Woohacked_textdomain');

	function Woohacked_textdomain()
	{
		load_plugin_textdomain(
			'woohacked',
			false,
			dirname( plugin_basename( __FILE__ )). '/languages'
		);		
	}

	class Woohacked
	{
		public function __construct()
		{
			$this->plugin_name = '⚡Woohacked';
			$this->plugin_author = 'Jaimelías';
			$this->plugin_author_url = 'https://jaimelias.com/';
			$this->plugin_url = 'https://woohacked.com/';
			$this->api_endpoint = 'https://woohacked.web.app/api/';
			$this->plugin_version = '1.0.0';
			$this->load_dependencies();
			$this->init();
		}
		
		public function init()
		{
			$this->load_dependencies();
			$this->load_classes();
			$this->actions_filters();
		}
		public function load_classes()
		{
			$this->api = new Woohacked_API($this->plugin_name, $this->api_endpoint);
			$this->settings = new Woohacked_Settings($this->plugin_name);
			$this->checkout = new Woohacked_Checkout($this->settings, $this->api->get_store_config());
			$this->cart = new Woohacked_Cart();
			$this->google_map = new Woohacked_Google_Map($this->settings, $this->api_endpoint);
			$this->storefront = new Woohacked_Storefront_tricks($this->settings, $this->plugin_name);
			$this->speed = new Woohacked_Speed();
			$this->credits = new Woohacked_Credits($this->plugin_name, $this->plugin_url, $this->plugin_author, $this->plugin_author_url);			
		}
		public function actions_filters()
		{
			add_action( 'woocommerce_shipping_init', 'Woohacked_Glovo_Init');
			add_action( 'woocommerce_shipping_methods', 'Woohacked_Glovo_Method_Init');
			add_action('wp_head', array(&$this, 'plugin_args'));			
		}
		
		public function load_dependencies ()
		{
			require_once(plugin_dir_path( __FILE__ ).'api.php');
			require_once(plugin_dir_path( __FILE__ ).'checkout.php');
			require_once(plugin_dir_path( __FILE__ ).'shipping/glovo.php');
			require_once(plugin_dir_path( __FILE__ ).'router/router.php');
			require_once(plugin_dir_path( __FILE__ ).'cart.php');
			require_once(plugin_dir_path( __FILE__ ).'google-map.php');
			require_once(plugin_dir_path( __FILE__ ).'storefront.php');
			require_once(plugin_dir_path( __FILE__ ).'settings.php');
			require_once(plugin_dir_path( __FILE__ ).'speed.php');
			require_once(plugin_dir_path( __FILE__ ).'credits.php');			
		}
		
		public function plugin_args()
		{
			$json = array();
			
			$json['dir_url'] = esc_url(plugin_dir_url( __FILE__ ));
			
			$script = '<script id="woohacked-args" type="application/json">'.json_encode(apply_filters('woohacked_args', $json)).'</script>';
			
			echo $script;	
		}
	}

	function Woohacked_Init()
	{
		$Woohacked = new Woohacked();
	}
	
	function Woohacked_Rewrite_Rules()
	{
		delete_option( 'rewrite_rules' );
	}
	
	add_action('plugins_loaded', 'Woohacked_Init');	
	register_activation_hook( __FILE__, 'Woohacked_Rewrite_Rules');	
	register_deactivation_hook( __FILE__, 'Woohacked_Rewrite_Rules');
}

