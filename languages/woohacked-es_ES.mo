��    6      �      |      |  {   }  M   �  N   G     �     �  
   �  
   �     �     �  5   �               /     ?     N     Z     a     w     �  !   �     �  
   �     �     �     �          +     G  
   `     k     z     �     �  !   �     �     �     �               )     <     K     Z  	   j  P   t  )   �     �     �       	             &     +  �  B  �   �	  ^   e
  n   �
  
   3     >  
   B     M     ^     n  ,   �     �     �     �     �                    2     L  )   i  )   �     �     �     �  /   �  .     7   B  /   z  
   �     �     �     �     �          &     8     F     S      k     �     �     �     �  	   �  U   �  +   <  
   h     s     �  	   �  
   �     �     �   %s is built with Wordpress, Woocommerce and the awesome e-commerce plugin %s, developed by the panamanian web developer %s. %s is built with Wordpress, Woocommerce and the awesome e-commerce plugin %s. %s lets any store in %s, %s to integrate %s and many other delivery providers. %s title 404 Breadcrumb Built with Calculate shipping Cart Settings Check the box if you want to activate %s in this zone City Click here to open map... Confirm address Contact Person Coordinates Design E-commerce & %s in %s E-commerce & %s in %s, %s Enable %s in this Zone? Find the exact address on the map Front Top Bar Glovo Keys Go Home Google Maps Hide city from the cart Hide country from the cart Hide postcode from the cart Hide state from the cart Jaimelías Online Shop %s Package Description Page Not Found Pickup Instructions Select a country / region&hellip; Select an option&hellip; Send with %s Share in Share your location Shipping Commision Site Notifications State / County Store Latitude Store Longitude Telephone The page you are looking for was moved, removed, renamed or might never existed. This is how you will call %s in your site Update Virtual Store %s Whatsapp Woohacked development from https://woohacked.com/ Project-Id-Version: Woocommerce Jaimelías
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-16 18:40+0000
PO-Revision-Date: 2020-08-16 18:40+0000
Last-Translator: 
Language-Team: Español
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 %s esta fue diseñado con Wordpress, Woocommerce y el genial app de commercio electrónico %s, creado por el desarrollador web panameño %s. %s esta fue diseñado con Wordpress, Woocommerce y el genial app de commercio electrónico %s. %s permite que cualquier tienda en %s, %s pueda integrar %s y muchos otros proveedores de entrega a domicilio. título %s 404 Breadcrumb Desarrollado con Calcular envío Configuración del carrito Selecciona si deseas activar %s en esta zona Corregimiento Haz click para ver mapa... Confirmar dirección Persona de contacto Coordenadas Diseño E-commerce y %s en %s E-commerce y %s en %s, %s ¿Habilitar %s en esta zona? Encuentra la ubicación exacta en el mapa Barra de notificación superior del sitio Claves Glovo Ir al inicio Google Maps Oculta la ciudad en la cálculadora del carrito Oculta el país en la cálculadora del carrito Oculta el código postal en la cálculadora del carrito Oculta el estado en la cálculadora del carrito Jaimelías %s de tiendas en línea Descripción del paquete Página no encontrada Instrucciones de recogida Elegir país... Elige una opción Enviar con %s Compartir en Compartir tu ubicación Comisión de entrega a domicilio Notificaciones del sitio web Región Latitud de la tienda Longitud de la tienda Teléfono Esta página que buscas fue movida, borrada, renombrada, o pudo nunca haber existido. Así es como llamarás a %s en tu sitio web Actualizar % de tiendas virtuales Whatsapp Woohacked desarrollo desde https://woohacked.com/ 